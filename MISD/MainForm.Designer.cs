﻿
namespace MISD {
    partial class MainForm {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataset_images_count_label = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.open_report_field = new System.Windows.Forms.CheckBox();
            this.report_path_button = new System.Windows.Forms.Button();
            this.report_path_field = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dataset_folder_field = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataset_select_dir_button = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.report_button = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lambda_label = new System.Windows.Forms.Label();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.L_field = new System.Windows.Forms.NumericUpDown();
            this.lambda_field = new System.Windows.Forms.NumericUpDown();
            this.K_field = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.max_glow = new System.Windows.Forms.NumericUpDown();
            this.min_glow = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.max_phi = new System.Windows.Forms.NumericUpDown();
            this.min_phi = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.max_sin = new System.Windows.Forms.NumericUpDown();
            this.min_sin = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.L_field)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lambda_field)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.K_field)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.max_glow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.min_glow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.max_phi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.min_phi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.max_sin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.min_sin)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataset_images_count_label);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.open_report_field);
            this.groupBox1.Controls.Add(this.report_path_button);
            this.groupBox1.Controls.Add(this.report_path_field);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.dataset_folder_field);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dataset_select_dir_button);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(567, 128);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Атлас";
            // 
            // dataset_images_count_label
            // 
            this.dataset_images_count_label.AutoSize = true;
            this.dataset_images_count_label.Location = new System.Drawing.Point(421, 93);
            this.dataset_images_count_label.Name = "dataset_images_count_label";
            this.dataset_images_count_label.Size = new System.Drawing.Size(13, 13);
            this.dataset_images_count_label.TabIndex = 8;
            this.dataset_images_count_label.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(290, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Изображений в атласе";
            // 
            // open_report_field
            // 
            this.open_report_field.AutoSize = true;
            this.open_report_field.Checked = true;
            this.open_report_field.CheckState = System.Windows.Forms.CheckState.Checked;
            this.open_report_field.Location = new System.Drawing.Point(13, 93);
            this.open_report_field.Name = "open_report_field";
            this.open_report_field.Size = new System.Drawing.Size(184, 17);
            this.open_report_field.TabIndex = 6;
            this.open_report_field.Text = "Открыть отчет после создания";
            this.open_report_field.UseVisualStyleBackColor = true;
            this.open_report_field.Visible = false;
            // 
            // report_path_button
            // 
            this.report_path_button.Location = new System.Drawing.Point(486, 58);
            this.report_path_button.Name = "report_path_button";
            this.report_path_button.Size = new System.Drawing.Size(75, 23);
            this.report_path_button.TabIndex = 5;
            this.report_path_button.Text = "Выбрать";
            this.report_path_button.UseVisualStyleBackColor = true;
            this.report_path_button.Visible = false;
            this.report_path_button.Click += new System.EventHandler(this.report_path_button_Click);
            // 
            // report_path_field
            // 
            this.report_path_field.Location = new System.Drawing.Point(102, 58);
            this.report_path_field.Name = "report_path_field";
            this.report_path_field.Size = new System.Drawing.Size(378, 20);
            this.report_path_field.TabIndex = 4;
            this.report_path_field.Visible = false;
            this.report_path_field.TextChanged += new System.EventHandler(this.report_path_field_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Путь отчета";
            this.label6.Visible = false;
            // 
            // dataset_folder_field
            // 
            this.dataset_folder_field.Location = new System.Drawing.Point(102, 20);
            this.dataset_folder_field.Name = "dataset_folder_field";
            this.dataset_folder_field.ReadOnly = true;
            this.dataset_folder_field.Size = new System.Drawing.Size(378, 20);
            this.dataset_folder_field.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Папка атласа";
            // 
            // dataset_select_dir_button
            // 
            this.dataset_select_dir_button.Location = new System.Drawing.Point(486, 20);
            this.dataset_select_dir_button.Name = "dataset_select_dir_button";
            this.dataset_select_dir_button.Size = new System.Drawing.Size(75, 23);
            this.dataset_select_dir_button.TabIndex = 0;
            this.dataset_select_dir_button.Text = "Выбрать";
            this.dataset_select_dir_button.UseVisualStyleBackColor = true;
            this.dataset_select_dir_button.Click += new System.EventHandler(this.dataset_select_dir_button_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(800, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(98, 17);
            this.toolStripStatusLabel1.Text = "Ожидание ввода";
            // 
            // report_button
            // 
            this.report_button.Location = new System.Drawing.Point(662, 52);
            this.report_button.Name = "report_button";
            this.report_button.Size = new System.Drawing.Size(104, 23);
            this.report_button.TabIndex = 2;
            this.report_button.Text = "Собрать отчет";
            this.report_button.UseVisualStyleBackColor = true;
            this.report_button.Click += new System.EventHandler(this.report_button_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.lambda_label);
            this.groupBox2.Controls.Add(this.numericUpDown4);
            this.groupBox2.Controls.Add(this.L_field);
            this.groupBox2.Controls.Add(this.lambda_field);
            this.groupBox2.Controls.Add(this.K_field);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(12, 146);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(304, 142);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Коэффициенты";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "δᵒ";
            this.label5.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "L";
            // 
            // lambda_label
            // 
            this.lambda_label.AutoSize = true;
            this.lambda_label.Location = new System.Drawing.Point(10, 57);
            this.lambda_label.Name = "lambda_label";
            this.lambda_label.Size = new System.Drawing.Size(12, 13);
            this.lambda_label.TabIndex = 2;
            this.lambda_label.Text = "λ";
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.Location = new System.Drawing.Point(178, 107);
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown4.TabIndex = 1;
            this.numericUpDown4.Visible = false;
            // 
            // L_field
            // 
            this.L_field.DecimalPlaces = 2;
            this.L_field.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.L_field.Location = new System.Drawing.Point(178, 81);
            this.L_field.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            131072});
            this.L_field.Name = "L_field";
            this.L_field.Size = new System.Drawing.Size(120, 20);
            this.L_field.TabIndex = 1;
            this.L_field.Value = new decimal(new int[] {
            6,
            0,
            0,
            131072});
            // 
            // lambda_field
            // 
            this.lambda_field.DecimalPlaces = 5;
            this.lambda_field.Increment = new decimal(new int[] {
            1,
            0,
            0,
            327680});
            this.lambda_field.Location = new System.Drawing.Point(178, 55);
            this.lambda_field.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.lambda_field.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            327680});
            this.lambda_field.Name = "lambda_field";
            this.lambda_field.Size = new System.Drawing.Size(120, 20);
            this.lambda_field.TabIndex = 1;
            this.lambda_field.Value = new decimal(new int[] {
            1,
            0,
            0,
            327680});
            // 
            // K_field
            // 
            this.K_field.Location = new System.Drawing.Point(178, 30);
            this.K_field.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.K_field.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.K_field.Name = "K_field";
            this.K_field.Size = new System.Drawing.Size(120, 20);
            this.K_field.TabIndex = 1;
            this.K_field.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.K_field.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Количество коэффициентов";
            this.label2.Visible = false;
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.max_glow);
            this.groupBox3.Controls.Add(this.min_glow);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.max_phi);
            this.groupBox3.Controls.Add(this.min_phi);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.max_sin);
            this.groupBox3.Controls.Add(this.min_sin);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(396, 146);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(370, 261);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Параметры качества";
            // 
            // max_glow
            // 
            this.max_glow.Location = new System.Drawing.Point(243, 195);
            this.max_glow.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.max_glow.Name = "max_glow";
            this.max_glow.Size = new System.Drawing.Size(120, 20);
            this.max_glow.TabIndex = 11;
            this.max_glow.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.max_glow.ValueChanged += new System.EventHandler(this.properties_values_changed);
            // 
            // min_glow
            // 
            this.min_glow.Location = new System.Drawing.Point(243, 169);
            this.min_glow.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.min_glow.Name = "min_glow";
            this.min_glow.Size = new System.Drawing.Size(120, 20);
            this.min_glow.TabIndex = 10;
            this.min_glow.ValueChanged += new System.EventHandler(this.properties_values_changed);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 197);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(161, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Максимальная светимость, %";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 171);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(155, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Минимальная светимость, %";
            // 
            // max_phi
            // 
            this.max_phi.DecimalPlaces = 6;
            this.max_phi.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.max_phi.Location = new System.Drawing.Point(243, 131);
            this.max_phi.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.max_phi.Name = "max_phi";
            this.max_phi.Size = new System.Drawing.Size(120, 20);
            this.max_phi.TabIndex = 7;
            this.max_phi.Visible = false;
            // 
            // min_phi
            // 
            this.min_phi.DecimalPlaces = 6;
            this.min_phi.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.min_phi.Location = new System.Drawing.Point(243, 105);
            this.min_phi.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.min_phi.Name = "min_phi";
            this.min_phi.Size = new System.Drawing.Size(120, 20);
            this.min_phi.TabIndex = 6;
            this.min_phi.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 133);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Максимальный φ";
            this.label9.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 107);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Минимальный φ";
            this.label10.Visible = false;
            // 
            // max_sin
            // 
            this.max_sin.DecimalPlaces = 6;
            this.max_sin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.max_sin.Location = new System.Drawing.Point(243, 61);
            this.max_sin.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.max_sin.Name = "max_sin";
            this.max_sin.Size = new System.Drawing.Size(120, 20);
            this.max_sin.TabIndex = 3;
            // 
            // min_sin
            // 
            this.min_sin.DecimalPlaces = 6;
            this.min_sin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.min_sin.Location = new System.Drawing.Point(243, 35);
            this.min_sin.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.min_sin.Name = "min_sin";
            this.min_sin.Size = new System.Drawing.Size(120, 20);
            this.min_sin.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Максимальный |sinδ|";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Минимальный |sinδ|";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.report_button);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "MISD";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.L_field)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lambda_field)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.K_field)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.max_glow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.min_glow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.max_phi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.min_phi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.max_sin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.min_sin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox dataset_folder_field;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button dataset_select_dir_button;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button report_button;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lambda_label;
        private System.Windows.Forms.NumericUpDown lambda_field;
        private System.Windows.Forms.NumericUpDown K_field;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.NumericUpDown L_field;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.CheckBox open_report_field;
        private System.Windows.Forms.Button report_path_button;
        private System.Windows.Forms.TextBox report_path_field;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label dataset_images_count_label;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown max_sin;
        private System.Windows.Forms.NumericUpDown min_sin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown max_phi;
        private System.Windows.Forms.NumericUpDown min_phi;
        private System.Windows.Forms.NumericUpDown max_glow;
        private System.Windows.Forms.NumericUpDown min_glow;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
    }
}

