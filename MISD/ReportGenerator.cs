﻿namespace MISD {

    /// <summary>
    /// Интерфейс генератора отчетов
    /// </summary>
    abstract class ReportGenerator {

        /// <summary>
        /// Конфигурации отчета
        /// </summary>
        public struct Configs {
            public int K;
            public double L; //толщина образца
            public double Lambda; //лямбда
            public double Min_PHI; //минимальное фи
            public double Max_PHI; //максимальное фи
            public double Min_SIN; //максимальный синус
            public double Max_SIN; //максимальный синус

        }

        /// <summary>
        /// Хранилище данных для отчета
        /// </summary>
        public class Data {

            /// <summary>
            /// Атлас
            /// </summary>
            public MISDDataset dataset;

            /// <summary>
            /// Конфигурации
            /// </summary>
            public Configs conf;


            public Data() {
                dataset = new MISDDataset();
                conf = new Configs();
            }
        }

        /// <summary>
        /// Запуск процесса генерации отчета
        /// </summary>
        /// <param name="data">данные для отчета</param>
        /// <param name="filepath">путь, по которому необходимо сохранить файл отчета</param>
        public abstract void Run(Data data, string filepath);
    }
}
