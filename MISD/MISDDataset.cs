﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace MISD {
    /// <summary>
    /// Класс для формирования атласа из файлов в папке
    /// </summary>
    class MISDDataset {

        /// <summary>
        /// 
        /// </summary>
        public class Coords {
            public int X {
                get; set;
            }
            public int Y {
                get; set;
            }
            public Coords() {
                X = 0;
                Y = 0;
            }
        }

        /// <summary>
        /// Абстракция элемента атласа
        /// </summary>
        public class DataPoint {
            /// <summary>
            /// Путь до файла 
            /// </summary>
            public string FilePath { get; set; }

            /// <summary>
            /// Свойство для получения имени файла из пути до файла
            /// </summary>
            public string Filename {
                get {
                    if (!string.IsNullOrEmpty(FilePath)) {
                        return Path.GetFileName(FilePath);
                    }
                    return null;
                }
            }

            /// <summary>
            /// Изменение размера изображения
            /// </summary>
            /// <param name="imgToResize"></param>
            /// <param name="size"></param>
            /// <returns></returns>
            private static System.Drawing.Image resizeImage(System.Drawing.Image imgToResize, Size size) {
                //Get the image current width  
                int sourceWidth = imgToResize.Width;
                //Get the image current height  
                int sourceHeight = imgToResize.Height;
                float nPercent = 0;
                float nPercentW = 0;
                float nPercentH = 0;
                //Calulate  width with new desired size  
                nPercentW = ((float)size.Width / (float)sourceWidth);
                //Calculate height with new desired size  
                nPercentH = ((float)size.Height / (float)sourceHeight);
                if (nPercentH < nPercentW)
                    nPercent = nPercentH;
                else
                    nPercent = nPercentW;
                //New Width  
                int destWidth = (int)(sourceWidth * nPercent);
                //New Height  
                int destHeight = (int)(sourceHeight * nPercent);
                Bitmap b = new Bitmap(destWidth, destHeight);
                Graphics g = Graphics.FromImage((System.Drawing.Image)b);
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                // Draw image with new width and height  
                g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
                g.Dispose();
                return (System.Drawing.Image)b;
            }

            /// <summary>
            /// Получение содержимого изображения с возможностью изменения размера
            /// </summary>
            /// <param name="img_size"></param>
            /// <returns></returns>
            public Image FileData(Size img_size = new Size()) {

                var image = Image.FromFile(FilePath);

                if (!img_size.IsEmpty) {
                    image = resizeImage(image, img_size);
                }

                //if (!string.IsNullOrEmpty(FilePath)) {
                //    return File.ReadAllBytes(FilePath);
                //}
                return image;

            }

            /// <summary>
            /// Список доминантных цветов для точки атласа
            /// </summary>
            public List<DominantColorCalculator.DominantColor> DominantColors { get; set; }


            //светимость
            public double GlowPercentage { get; set; }

            /// <summary>
            /// Кординаты точки в атласе
            /// </summary>
            public Coords Coord { get; set; }

            /// <summary>
            /// Перегрузка метода сравнения
            /// </summary>
            /// <param name="obj"></param>
            /// <returns></returns>
            public override bool Equals(object obj) => this.Equals(obj as DataPoint);

            public bool Equals(DataPoint p) {
                if (p is null) {
                    return false;
                }

                // Optimization for a common success case.
                if (Object.ReferenceEquals(this, p)) {
                    return true;
                }

                // If run-time types are not exactly the same, return false.
                if (this.GetType() != p.GetType()) {
                    return false;
                }

                // Return true if the fields match.
                // Note that the base class is not invoked because it is
                // System.Object, which defines Equals as reference equality.
                return (Coord.X == p.Coord.X) && (Coord.Y == p.Coord.Y);
            }

            public override int GetHashCode() => (Coord.X, Coord.Y).GetHashCode();

            public static bool operator ==(DataPoint lhs, DataPoint rhs) {
                if (lhs is null) {
                    if (rhs is null) {
                        return true;
                    }

                    // Only the left side is null.
                    return false;
                }
                // Equals handles case of null on right side.
                return lhs.Equals(rhs);
            }

            public static bool operator !=(DataPoint lhs, DataPoint rhs) => !(lhs == rhs);
        }


        /// <summary>
        /// Хранилище точек 
        /// </summary>
        private List<DataPoint> m_storage = new List<DataPoint>();


        /// <summary>
        /// Общая ширина атласа
        /// </summary>
        public int Width {
            get; private set;
        }

        /// <summary>
        /// Общая высота атласа
        /// </summary>
        public int Height {
            get; private set;
        }

        public int Rows { get; set; }
        public int Columns { get; set; }

        /// <summary>
        /// Свойство для публичного доступа к хранилищу точек
        /// </summary>
        public List<DataPoint> Points {
            get {
                return m_storage;
            }
        }

        /// <summary>
        /// Конструктор атласа
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public MISDDataset(int width = 0, int height = 0) {
            Width = width;
            Height = height;

            m_storage.Clear();
        }

        /// <summary>
        /// Свойство для получения количества точек атласа
        /// </summary>
        public int ElementsCount {
            get {
                return m_storage.Count;
            }
        }

        /// <summary>
        /// Добавление точки а втлас
        /// </summary>
        /// <param name="point">точка</param>
        public void AddPoint(DataPoint point) {
            //проверяем, что такой точки нет в списке
            if (!m_storage.Contains(point)) {
                m_storage.Add(point);

                if (point.Coord.X > Width) {
                    Width = point.Coord.X;
                }
                if (point.Coord.Y > Height) {
                    Height = point.Coord.Y;
                }
            }
        }

        /// <summary>
        /// Сортировка хранлища точек для укладки точек в таблицу
        /// </summary>
        private void Sort() {

            //получаем уникальные Y координаты
            var uniqueY = m_storage.GroupBy(o => o.Coord.Y).Select(g => g.First()).OrderBy(o => o.Coord.Y).ToList();

            //количество строк равно количеству уникальных Y
            Rows = uniqueY.Count;

            List<DataPoint> result_copy = new List<DataPoint>(); //список для отсортированного варианта хранилища
            foreach (var y in uniqueY) { //обходим список уникальных Y
                //из списка точек получаем список точек, у которых текущая Y и сортируем их по возрастанию
                List<DataPoint> row = m_storage.Where(o => (o.Coord.Y == y.Coord.Y)).OrderBy(o => o.Coord.X).ToList();
                result_copy.AddRange(row);
                //количество колонок равно максимум количеству элементов в строке
                Columns = Math.Max(row.Count, Columns);
            }
            //переписываем результат  в хранилище
            m_storage = result_copy;
        }

        /// <summary>
        /// Статический метод для извлечения координат из имени файла
        /// </summary>
        /// <param name="filename">имя файла</param>
        /// <returns></returns>
        static Coords export_coords1(string filename) {
            //если имя файла пустое, то значит в нём ничего и нет
            if (string.IsNullOrEmpty(filename)) {
                return null;
            }

            //шаблон регулярного выражения для извлечения. 
            string pattern = @"^(\d+)_(\d+)\.(?:png|jpeg|jpg)$";
            Match m = Regex.Match(filename, pattern, RegexOptions.IgnoreCase);
            if (m.Success) {
                int x = Convert.ToInt32(m.Groups[1].Value);
                int y = Convert.ToInt32(m.Groups[2].Value);

                var res = new Coords();
                res.X = x;
                res.Y = y;

                return res;
            }
            return null;

        }

        static Coords export_coords(string filename) {
            //если имя файла пустое, то значит в нём ничего и нет
            if (string.IsNullOrEmpty(filename)) {
                return null;
            }

            //шаблон регулярного выражения для извлечения. 
            string pattern = @"^(\d+)\.(?:png|jpeg|jpg)$";
            Match m = Regex.Match(filename, pattern, RegexOptions.IgnoreCase);
            if (m.Success) {
                int x = Convert.ToInt32(m.Groups[1].Value);
                int y = 0;

                var res = new Coords();
                res.X = x;
                res.Y = y;

                return res;
            }
            return null;

        }

        /// <summary>
        /// Статический метод извлечения атласа из папки
        /// </summary>
        /// <param name="folder_path">путь до папки</param>
        /// <returns></returns>
        public static MISDDataset FromFolder(string folder_path) {

            //получаем коллекцию файлов в папке
            string[] files = Directory.GetFiles(folder_path);

            //заводим спискок для всех найденных точек
            List<DataPoint> all_coords = new List<DataPoint>();


            //обходим коллекцию файлов в папке
            foreach (string filepath in files) {
                //извлекаем имя файла из пути
                var filename = Path.GetFileName(filepath);
                filename = filename.ToLower();
                if (string.IsNullOrEmpty(filename)) {
                    continue;
                }

                //пытаемся извлечь координаты из имени файла
                var coords = export_coords(filename);
                if (coords == null) { //если координаты не найдены, идём дальше
                    continue;
                }

                //формируем новую точку атласа и заполняем поля
                var point = new DataPoint();
                point.Coord = coords;
                point.FilePath = filepath;
                //добавляем точку в список
                all_coords.Add(point);
            }

            //создаем новый атлас
            var set = new MISDDataset();

            //обходим сформированный список точек и добавляем в атлас
            foreach (var point in all_coords) {
                set.AddPoint(point);
            }

            //сортируем атлас
            set.Sort();

            return set;
        }

    }
}
