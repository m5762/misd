//подключение пространств имён 
using Accord.Imaging.Filters;
using Accord.Math;
using Accord.Math.Distances;
using ColorMine.ColorSpaces;
using ColorMine.ColorSpaces.Comparisons;
using System.Drawing;
using System;

namespace MISD {

    /// <summary>
    /// Класс расчета доминантных цветов изображения
    /// </summary>
    class DominantColorCalculator {

        /// <summary>
        /// Умолчательное минимальное значение 
        /// </summary>
        private static double MIN = -1.0;

        /// <summary>
        /// Умолчательное максимальное значение
        /// </summary>
        private static double MAX = 1.0;


        /// <summary>
        /// Результаты расчета
        /// </summary>
        public class DominantColor {
            /// <summary>
            /// Цвет
            /// </summary>
            public Color Color { get; set; }

            /// <summary>
            /// Процент покрытия. От 0 до 1
            /// </summary>
            public double Coverrage { get; set; }

            public DominantColor() {
                Color = Color.Black;
                Coverrage = 0.0;
            }

            /// <summary>
            /// Метод генерации изображения с указанным цветов
            /// </summary>
            /// <param name="width">ширина изображения</param>
            /// <param name="heidht">высота изображения</param>
            /// <returns></returns>
            public Bitmap GenerateImage(int width, int heidht) {
                var bitmap = new Bitmap(width, heidht);
                Graphics g = Graphics.FromImage(bitmap);
                g.Clear(Color);
                return bitmap;
            }
        }


        /// <summary>
        /// Класс расчета дистанции между цветами CIE1976
        /// </summary>
        class CIEFilter : IDistance<double[]> {

            /// <summary>
            /// Метод расчета дистанции между пикселями x и y
            /// </summary>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <returns></returns>
            public double Distance(double[] x, double[] y) {

                //преобразуем значения RGB из относительных в абсолютные для 24х битного цветового пространства
                var a_vals = Vector.Scale(x, MIN, MAX, 0, 255);
                var b_vals = Vector.Scale(y, MIN, MAX, 0, 255);

                //получаем цвета пикселей в цветовом пространстве RGB 24 бита
                var pixelA = new Rgb { R = a_vals[1], G = a_vals[2], B = a_vals[3] };
                var pixelB = new Rgb { R = b_vals[1], G = b_vals[2], B = b_vals[3] };

                //получаем дистанцию между цветами
                var distance = pixelA.Compare(pixelB, new Cie1976Comparison());

                distance = Vector.Scale(distance, 0.0, 255.0, MIN, MAX);

                return distance;
            }
        }


        public static double GlowPercentage(string filepath, int min_g = 0, int max_g = 255) {

            var image = new Bitmap(System.Drawing.Image.FromFile(filepath));

            //размываем изображение
            image = new Median(3).Apply(image);

            //делаем изображение монохромным 
            image = Accord.Imaging.Filters.Grayscale.CommonAlgorithms.BT709.Apply(image);

            Decimal total_score = 0; //общий счёт
            Decimal image_score = 0; //общий счёт
            int max_glow = 0; //значение самого яркого пикселя
            

            long cnt = 0;

            for (var x = 0; x < image.Width; x++) {
                for (var y = 0; y < image.Height; y++) {
                    var pixel = image.GetPixel(x, y);
                    var glow = pixel.G; //извлекаем только одну компоненту, так как все они одинаковые
                    max_glow = Math.Max(glow, max_glow);
                }
            }
            total_score = (image.Width * image.Height ) * max_glow;

            for (var x = 0; x < image.Width; x++) {
                for (var y = 0; y < image.Height; y++) {
                    var pixel = image.GetPixel(x, y);
                    var glow = pixel.G; //извлекаем только одну компоненту, так как все они одинаковые
                    image_score += glow;
                }
            }






            double percentage = (double)(image_score/total_score);


            return percentage;
        }
    }
}
