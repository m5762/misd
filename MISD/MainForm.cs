﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MISD {
    /// <summary>
    /// Основная форма 
    /// </summary>
    public partial class MainForm : Form {

        private MISDDataset m_dataset = new MISDDataset(); //атлас 
        public MainForm() {
            InitializeComponent();
            report_button.Enabled = false;
            report_path_field.Text = MISD.Properties.Settings.Default.LastReportPath;
            RestoreFromProperties();
            ConnectWatchQualityFields();
        }

        //подключаем обработчики событий изменений значений полей настройки
        void ConnectWatchQualityFields() {
            max_phi.ValueChanged += this.properties_values_changed;
            min_phi.ValueChanged += this.properties_values_changed;

            max_sin.ValueChanged += this.properties_values_changed;
            min_sin.ValueChanged += this.properties_values_changed;

            K_field.ValueChanged += this.properties_values_changed;
            L_field.ValueChanged += this.properties_values_changed;
            lambda_field.ValueChanged += this.properties_values_changed;
        }

        //сохранение настроек в памяти
        void SaveProperties() {
            MISD.Properties.Settings.Default.Max_PHI = max_phi.Value;
            MISD.Properties.Settings.Default.Min_PHI = min_phi.Value;

            MISD.Properties.Settings.Default.Max_SIN = max_sin.Value;
            MISD.Properties.Settings.Default.Min_SIN = min_sin.Value;

            MISD.Properties.Settings.Default.K = K_field.Value;
            MISD.Properties.Settings.Default.L = L_field.Value;
            MISD.Properties.Settings.Default.Lambda = lambda_field.Value;

            MISD.Properties.Settings.Default.MinGlow = min_glow.Value;
            MISD.Properties.Settings.Default.MaxGlow = max_glow.Value;


            MISD.Properties.Settings.Default.Save();
        }

        //восстановление настроек из памяти
        void RestoreFromProperties() {
            max_sin.Value = MISD.Properties.Settings.Default.Max_SIN;
            min_sin.Value = MISD.Properties.Settings.Default.Min_SIN;

            max_phi.Value = MISD.Properties.Settings.Default.Max_PHI;
            min_phi.Value = MISD.Properties.Settings.Default.Min_PHI;

            K_field.Value = MISD.Properties.Settings.Default.K;
            lambda_field.Value = MISD.Properties.Settings.Default.Lambda;
            L_field.Value = MISD.Properties.Settings.Default.L;

            min_glow.Value = MISD.Properties.Settings.Default.MinGlow;
            max_glow.Value = MISD.Properties.Settings.Default.MaxGlow;
        }

        /// <summary>
        /// обработчик кнопки выбора директории атласа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataset_select_dir_button_Click(object sender, EventArgs e) {
            using (var filedialog = new FolderBrowserDialog()) {
                filedialog.ShowNewFolderButton = false;
                filedialog.Description = "Выберите папку, содержащую атлас";
                filedialog.SelectedPath = MISD.Properties.Settings.Default.LastDatasetPath;

                DialogResult result = filedialog.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(filedialog.SelectedPath)) { //исли нажата кнопка ОК
                    //переносим сохрнённый путь в поле пути
                    dataset_folder_field.Text = filedialog.SelectedPath;

                    //сохраняем путь в настройках
                    MISD.Properties.Settings.Default.LastDatasetPath = filedialog.SelectedPath;
                    MISD.Properties.Settings.Default.Save();

                    //формируем атлас из пути
                    var dataset = MISDDataset.FromFolder(filedialog.SelectedPath);

                    m_dataset = dataset;

                    //формируем путь до файла отчета
                    string reportPath = Path.Combine(filedialog.SelectedPath, "report.xls");
                    report_path_field.Text = reportPath;

                }
            }

            //вычисляем состояние кнопки формирвоания отчета
            CalculateReportButton();
            //показыаем количество 
            UpdateDatasetCountLabel();


        }

        /// <summary>
        /// Обработчик кнопки формирования отчета
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void report_button_Click(object sender, EventArgs e) {
            //запуск фоновой задачи, чтобы не тормозить графический интерфейс
            backgroundWorker1.RunWorkerAsync();

        }

        private bool worker_success = false;

        /// <summary>
        /// обработчик фоновой задачи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e) {
            worker_success = false;

            //отображаем в поле статуса состояние
            this.Invoke((MethodInvoker)delegate {
                this.Enabled = false;
                toolStripStatusLabel1.Text = "Формирую отчет";
            });

            //создаем объект формирования отчет
            XLSReportGenerator.Data reportData = new XLSReportGenerator.Data();
            //заполняем объект атласом
            reportData.dataset = m_dataset;

            int k = (int)K_field.Value;

            int min_glow_percentage = (int)((255.0 / 100.0) * (double)min_glow.Value);
            int max_glow_percentage = (int)((255.0 / 100.0) * (double)max_glow.Value);

            //считаем для каждой точки доминантные цвета
            /*foreach (var point in m_dataset.Points) {
                //var colors = DominantColorCalculator.DomanantColors(point.FilePath, k);
                //DominantColorCalculator.GlowPercentage(point.FilePath, 40, 255);
                //point.DominantColors = colors;

                int min_glow_percentage = (int)((255.0 / 100.0) * (double)min_glow.Value);
                int max_glow_percentage = (int)((255.0 / 100.0) * (double)max_glow.Value);

                double glow = DominantColorCalculator.GlowPercentage(point.FilePath, min_glow_percentage, max_glow_percentage);
                point.GlowPercentage = glow;
            }*/

            Parallel.ForEach(m_dataset.Points, point => {
                double glow = DominantColorCalculator.GlowPercentage(point.FilePath, min_glow_percentage, max_glow_percentage);
                point.GlowPercentage = glow;
            });


            reportData.conf.K = (int)K_field.Value;
            reportData.conf.L = (double)L_field.Value;
            reportData.conf.Lambda = (double)lambda_field.Value;

            reportData.conf.Min_PHI = (double)min_phi.Value;
            reportData.conf.Max_PHI = (double)max_phi.Value;
            reportData.conf.Max_SIN = (double)max_sin.Value;
            reportData.conf.Min_SIN = (double)min_sin.Value;

            try {
                new XLSReportGenerator().Run(reportData, report_path_field.Text);
                worker_success = true;
            }
            catch (System.IO.IOException) {
                this.Invoke((MethodInvoker)delegate {
                    worker_success = false;
                    this.Enabled = true;
                    toolStripStatusLabel1.Text = "Ошибка формирования отчета";
                    MessageBox.Show("Проверьте, что отчет не открыт в другой программе", "Ошибка открытия файла", MessageBoxButtons.OK, MessageBoxIcon.Error);
                });

            }



        }

        /// <summary>
        /// Обработчик окончания работы фоновой задачи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

            if (!worker_success) {
                return;
            }
            this.Invoke((MethodInvoker)delegate {
                this.Enabled = true;
                toolStripStatusLabel1.Text = "Создание отчета завершено";
            });

            if (open_report_field.Checked) {
                //открываем отчет в программе по умолчанию
                OpenWithDefaultProgram(report_path_field.Text);
            }

        }


        /// <summary>
        /// Открытие файла в программе по умолчанию
        /// </summary>
        /// <param name="path"></param>
        private static void OpenWithDefaultProgram(string path) {
            Process fileopener = new Process();

            fileopener.StartInfo.FileName = "explorer";
            fileopener.StartInfo.Arguments = "\"" + path + "\"";
            fileopener.Start();
        }

        private void report_path_button_Click(object sender, EventArgs e) {
            using (var filedialog = new SaveFileDialog()) {
                filedialog.AddExtension = true;
                filedialog.Filter = "XLS (*.xls)|*.xls";
                filedialog.CheckFileExists = true;


                DialogResult result = filedialog.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(filedialog.FileName)) {
                    MISD.Properties.Settings.Default.LastReportPath = filedialog.FileName;
                    MISD.Properties.Settings.Default.Save();
                    report_path_field.Text = filedialog.FileName;
                }
            }

            CalculateReportButton();
        }


        void CalculateReportButton() {
            if ((m_dataset.ElementsCount <= 0)) {
                report_button.Enabled = false;
                return;
            }

            if (report_path_field.Text.Length == 0) {
                report_button.Enabled = false;
                return;
            }


            report_button.Enabled = true;
        }


        void UpdateDatasetCountLabel() {
            dataset_images_count_label.Text = m_dataset.ElementsCount.ToString();
        }

        private void report_path_field_TextChanged(object sender, EventArgs e) {
            MISD.Properties.Settings.Default.LastReportPath = report_path_field.Text;
            MISD.Properties.Settings.Default.Save();
        }

        private void properties_values_changed(object sender, EventArgs e) {
            SaveProperties();
        }

    }
}
