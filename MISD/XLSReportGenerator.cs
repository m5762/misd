﻿using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Drawing;
using System.IO;

namespace MISD {
    /// <summary>
    /// Класс генерации отчета в формате XLS
    /// </summary>
    class XLSReportGenerator : ReportGenerator {

        //константы номеров колонок столбцов
        private const int COL_PP = 0;
        private const int COL_FILENAME = 1;
        private const int COL_ANGLE = 2;
        private const int COL_IMAGE = 3;
        private const int COL_WIDTH = 4;
        private const int COL_I = 5;
        private const int COL_OPTIC_ANIZOTROPY = 6;
        private const int COL_QUALITY_SIN = 7;


        private const int MAX_COLS = 8; //максимальное количество колонок



        //шаблоны формул ячеекк с формулами
        private const string ORIENTATION_FORMULA_TEMPLATE = "ASIN((SQRT(${0}${1}/((SIN({2:F6}/2))^2))))";
        private const string ANIZOTROPY_FORMULA_TEMPLATE = "2*(ATAN(SQRT(${0}${1})))";
        private const string QUALITY_SIN_FORMULA_TEMPLATE = "IF(AND(${0}{1}>={2:F6}, ${0}{1}<={3:F6}),\"УДВОЛЕТВОРЯЕТ\", \"НЕ УДВОЛЕТВОРЯЕТ\")";


        /// <summary>
        /// Формирование отчета
        /// </summary>
        /// <param name="data"></param>
        /// <param name="filepath"></param>
        public override void Run(Data data, string filepath) {

            string filename = filepath;
            //создаём элемент электронной книги
            var workbook = new HSSFWorkbook();

            var report_sheet = GenerateHeader(workbook);
            ProcessBody(data, report_sheet);

            for (int i = 0; i < MAX_COLS; i++) {
                report_sheet.AutoSizeColumn(i, true);
            }

            using (var fileData = new FileStream(filename, FileMode.Create)) {
                workbook.Write(fileData);
            }
        }

        /// <summary>
        /// Создание стиля ячейки с прорисованными границами
        /// </summary>
        /// <param name="workbook"></param>
        /// <returns></returns>
        private ICellStyle BorderedCellStyle(IWorkbook workbook) {
            ICellStyle blackBorder = workbook.CreateCellStyle();
            blackBorder.BorderBottom = BorderStyle.Thin;
            blackBorder.BorderLeft = BorderStyle.Thin;
            blackBorder.BorderRight = BorderStyle.Thin;
            blackBorder.BorderTop = BorderStyle.Thin;

            blackBorder.BottomBorderColor = HSSFColor.Black.Index;
            blackBorder.LeftBorderColor = HSSFColor.Black.Index;
            blackBorder.RightBorderColor = HSSFColor.Black.Index;
            blackBorder.TopBorderColor = HSSFColor.Black.Index;

            blackBorder.Alignment = HorizontalAlignment.CenterSelection;
            blackBorder.VerticalAlignment = VerticalAlignment.Center;
            return blackBorder;
        }

        /// <summary>
        /// Формирование шапки таблицы и страницы результатов
        /// </summary>
        /// <param name="workbook"></param>
        /// <returns></returns>
        private ISheet GenerateHeader(HSSFWorkbook workbook) {
            //создаём страницу 
            var sheet = workbook.CreateSheet("Обработка");

            //создаём строку с индексом 0
            var row = sheet.CreateRow(0);

            //создание всех ячеек в первой строке
            for (int i = 0; i < MAX_COLS; i++) {
                row.CreateCell(i);
            }

            //получаем стиль для ячеек шапки и применяем на него полужирный шрифт
            var blackBorder = BorderedCellStyle(workbook);

            var font = workbook.CreateFont();
            font.IsBold = true;
            blackBorder.SetFont(font);

            //заполняем первую строку шапки
            row.GetCell(COL_PP).SetCellValue("п/п");

            row.GetCell(COL_FILENAME).SetCellValue("Файл");

            row.GetCell(COL_ANGLE).SetCellValue("Угол поворота");

            row.GetCell(COL_IMAGE).SetCellValue("Изображение");

            row.GetCell(COL_WIDTH).SetCellValue("Толщина образца, мм");

            row.GetCell(COL_I).SetCellValue("Относительная интенсивность, I");

            row.GetCell(COL_OPTIC_ANIZOTROPY).SetCellValue("Оптическая анизотропия, |sinδ|");

            row.GetCell(COL_QUALITY_SIN).SetCellValue("         Вывод по |sinδ|         ");


            //применяем стиль шапки во всем ячейкам строки шапки
            foreach (var cell in row.Cells) {
                cell.CellStyle = blackBorder;
            }

            //требуем от каждого столбца, чтобы он сам выровнялся по ширине контента
            for (int i = 0; i < MAX_COLS; i++) {
                sheet.AutoSizeColumn(i, true);
            }

            sheet.SetColumnWidth(COL_QUALITY_SIN, sheet.GetColumnWidth(COL_QUALITY_SIN) * 4);
            sheet.SetColumnWidth(COL_IMAGE, sheet.GetColumnWidth(COL_IMAGE) * 3);


            return sheet;
        }

        /// <summary>
        /// Преобразование изображения в последовательность байт
        /// </summary>
        /// <param name="imageIn"></param>
        /// <returns></returns>
        private static byte[] ImageToByteArray(System.Drawing.Image imageIn) {
            ImageConverter _imageConverter = new ImageConverter();
            byte[] xByte = (byte[])_imageConverter.ConvertTo(imageIn, typeof(byte[]));
            return xByte;
        }

        /// <summary>
        /// Метод получения буквы столца из его номера
        /// </summary>
        /// <param name="col">номер столбца</param>
        /// <returns></returns>
        private static string LetterFromColumn(int col) {
            return CellReference.ConvertNumToColString(col);
        }


        void ProcessBody(Data data, ISheet sheet) {
            int PP = 1; //порядковый номер
            int row_no = 1;

            foreach (var point in data.dataset.Points) {
                //создаём строку для заполнения данными
                var row1 = sheet.CreateRow(row_no);

                //получаем стиль для ячеек
                var blackBorder = BorderedCellStyle(sheet.Workbook);

                //создаём все ячейки в строке и сразу заполняем их пустыми занчениями
                for (int i = 0; i < MAX_COLS; i++) {
                    row1.CreateCell(i).CellStyle = blackBorder;
                    row1.GetCell(i).SetCellValue("");
                }

                //заполняем поле порядкового номера
                row1.GetCell(COL_PP).SetCellValue(PP++);

                //заполняем поле имени файла
                row1.GetCell(COL_FILENAME).SetCellValue(point.Filename);

                //угол поворота
                row1.GetCell(COL_ANGLE).SetCellValue(point.Coord.X);

                //толщина образца
                row1.GetCell(COL_WIDTH).SetCellValue(data.conf.L);
                row1.GetCell(COL_WIDTH).SetCellType(CellType.Numeric);

                //относительная интесивность
                row1.GetCell(COL_I).SetCellValue(point.GlowPercentage);
                row1.GetCell(COL_I).SetCellType(CellType.Numeric);

                var cell = row1.GetCell(COL_OPTIC_ANIZOTROPY);
                string cellValue = String.Format(new System.Globalization.CultureInfo("en-GB"), ANIZOTROPY_FORMULA_TEMPLATE, LetterFromColumn(COL_I), cell.RowIndex + 1);
                cell.SetCellFormula(cellValue);


                cell = row1.GetCell(COL_QUALITY_SIN);
                cellValue = String.Format(new System.Globalization.CultureInfo("en-GB"), QUALITY_SIN_FORMULA_TEMPLATE, LetterFromColumn(COL_OPTIC_ANIZOTROPY), cell.RowIndex + 1, data.conf.Min_SIN, data.conf.Max_SIN);
                cell.SetCellFormula(cellValue);


                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(row1.RowNum, row1.RowNum + 4, COL_IMAGE, COL_IMAGE));


                //оригинальное изображение


                var image = point.FileData(new System.Drawing.Size(50, 50));

                int pictureIndex = sheet.Workbook.AddPicture(ImageToByteArray(image), PictureType.JPEG);
                ICreationHelper helper = sheet.Workbook.GetCreationHelper();
                IDrawing drawing = sheet.CreateDrawingPatriarch();
                IClientAnchor anchor = helper.CreateClientAnchor();
                anchor.Col1 = COL_IMAGE;
                anchor.Row1 = row1.RowNum;

                anchor.AnchorType = AnchorType.DontMoveAndResize;

                IPicture picture = drawing.CreatePicture(anchor, pictureIndex);

                Size size = picture.GetImageDimension();


                picture.Resize(1, 5);


                row_no += 6;
            }
        }
    }
}
