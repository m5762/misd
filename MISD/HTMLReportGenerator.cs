﻿using RazorEngine;
using RazorEngine.Templating; // For extension methods.
using System.IO;

namespace MISD {
    class HTMLReportGenerator : ReportGenerator {
        public override void Run(Data data, string filepath) {
            //string template = "<div>Hello @Model.Name</div>";
            string template = File.ReadAllText("template.html");
            var model = new { Name = "Matt" };

            string result = Engine.Razor.RunCompile(template, "key", typeof(Data), data);

            using (var fileData = new StreamWriter(filepath)) {
                fileData.Write(result);
            }
        }
    }
}
